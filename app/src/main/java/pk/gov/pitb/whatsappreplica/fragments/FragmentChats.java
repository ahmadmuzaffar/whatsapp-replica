package pk.gov.pitb.whatsappreplica.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orm.SugarContext;

import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pk.gov.pitb.whatsappreplica.R;
import pk.gov.pitb.whatsappreplica.activities.ActivityMain;
import pk.gov.pitb.whatsappreplica.adapters.ArrayAdapterChats;
import pk.gov.pitb.whatsappreplica.helper.Globals;
import pk.gov.pitb.whatsappreplica.models.ClassChats;
import pk.gov.pitb.whatsappreplica.models.OnSwipeTouchListener;

public class FragmentChats extends Fragment {

    RecyclerView recyclerView;
    TextView emptyView;
    List<ClassChats> classChatsList;
    ArrayAdapterChats arrayAdapterChats;
    Activity activity;

    @Override
    public void onResume() {

        setAdapter();

        super.onResume();
    }

    private void setAdapter() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        classChatsList = ClassChats.listAll(ClassChats.class);

        if (classChatsList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        arrayAdapterChats = new ArrayAdapterChats(classChatsList, getActivity());

        recyclerView.setAdapter(arrayAdapterChats);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_chats, container, false);

        SugarContext.init(getContext());

        recyclerView = view.findViewById(R.id.list_chats);
        emptyView = view.findViewById(R.id.emptyList);
        activity = getActivity();

//        ClassChats obj1, obj2;
//        obj1 = new ClassChats();
//        obj1.setName("Ali");
//        obj1.setLastMessage("Bye Bye");
//        obj1.setMessageTime("12:54 pm");
//        obj1.save();
//
//        obj2 = new ClassChats();
//        obj2.setName("Adil");
//        obj2.setLastMessage("Bye Bye");
//        obj2.setMessageTime("02:15 pm");
//        obj2.save();

        return view;

    }

//    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP |
//            ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//
//        @Override
//        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//            if(direction == ItemTouchHelper.RIGHT){
//                if(currentTab.equals("chats")){
////                    openCamera();
//                }else if(currentTab.equals("status")){
//                    tab_chats.performClick();
//                }else if(currentTab.equals("calls")){
//                    tab_status.performClick();
//                }
//            } else if(direction == ItemTouchHelper.LEFT){
//                if(currentTab.equals("chats")){
//                    tab_status.performClick();
//                }else if(currentTab.equals("status")){
//                    tab_calls.performClick();
//                }
//            }
//        }
//
//        @Override
//        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
//            int swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
//            return makeMovementFlags(dragFlags, swipeFlags);
//        }
//
//        @Override
//        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//            return false;
//        }
//    };

}

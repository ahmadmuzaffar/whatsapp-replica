package pk.gov.pitb.whatsappreplica.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.orm.SugarContext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import pk.gov.pitb.whatsappreplica.R;
import pk.gov.pitb.whatsappreplica.adapters.PageAdapter;

public class ActivityMain extends AppCompatActivity {

    TabItem tab_camera, tab_chats, tab_status, tab_calls;
    ViewPager viewPager;
    TabLayout tabLayout;
    PageAdapter pageAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_screen_actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                /* DO EDIT */
                return true;
            case R.id.menu_newGroup:
                /* DO ADD */
                return true;
            case R.id.menu_newBroadcast:
                /* DO DELETE */
                return true;
            case R.id.menu_whatsappWeb:
                /* DO DELETE */
                return true;
            case R.id.menu_starredMessages:
                /* DO DELETE */
                return true;
            case R.id.menu_settings:
                /* DO DELETE */
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SugarContext.init(ActivityMain.this);
        generateBody();

    }

    @Override
    public void onBackPressed() {
        if(viewPager.getCurrentItem() == 2 || viewPager.getCurrentItem() == 3){
            viewPager.setCurrentItem(1);
        }else{
            super.onBackPressed();
        }
    }

    private void generateBody() {

        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
        getSupportActionBar().setElevation(0);

        tab_camera = findViewById(R.id.tab_camera);
        tab_chats = findViewById(R.id.tab_chats);
        tab_status = findViewById(R.id.tab_status);
        tab_calls = findViewById(R.id.tab_calls);
        viewPager = findViewById(R.id.fragmentContainer);
        tabLayout = findViewById(R.id.tabs);

        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }
}

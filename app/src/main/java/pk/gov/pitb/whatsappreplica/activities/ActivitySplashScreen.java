package pk.gov.pitb.whatsappreplica.activities;

import androidx.appcompat.app.AppCompatActivity;
import pk.gov.pitb.whatsappreplica.R;

import android.content.Intent;
import android.os.Bundle;

import com.orm.SugarContext;

public class ActivitySplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        SugarContext.init(ActivitySplashScreen.this);
        setToWait();

    }

    public void setToWait(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1500);

                    startMainActivity();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    private void startMainActivity() {

        startActivity(new Intent(ActivitySplashScreen.this, ActivityMain.class));
        finish();

    }
}

package pk.gov.pitb.whatsappreplica.helper;

import java.util.ArrayList;
import java.util.Arrays;

public class Constants {
//	public static final String FONT_URDU = "fonts/nafees_web.ttf";

	/********************************* LIVE *********************************/

	/* LIVE BETA */public static String BASE_URL = "";

	/**********************************************************************/

	public static final String FORMAT_TIME_APP = "HH:mm:ss";
	public static final String FORMAT_DATE_APP = "yyyy-MM-dd";
	public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String VIEW_FORMAT_DATE_TIME = "dd-MM-yyyy hh:mm a";

	public static final int AC_DO_NOTHING = -1;
	public static final int AC_SHOW_DIALOG = 0;
	public static final int AC_NO_INTERNET_CONNECTED = 1;
	public static final int AC_NO_INTERNET_WORKING = 2;
	public static final int AC_SHOW_PROGRESS_DIALOG = 3;
	public static final int AC_DISMISS_PROGRESS_DIALOG = 4;
	public static final int AC_TOWNS_DIALOG_DISMISSED = 5;
	public static final int AC_SUBMIT_SUCCESS = 11;
	public static final int AC_SUBMIT_ERROR = 12;
	public static final int AC_SUBMIT_EXCEPTION = 13;
	public static final int AC_SUBMIT_FINISH_ACTIVITY = 14;
	public static final int AC_SUBMIT_NO_INTERNET = 15;
	public static final int AC_SYNC_SUCCESS = 21;
	public static final int AC_SYNC_ERROR = 22;
	public static final int AC_SYNC_EXCEPTION = 23;
	public static final int AC_SYNC_NO_DATA = 24;
	public static final int AC_NOT_REGISTERED = 25;

	public static final String ACTIVITY_TAG_PATIENT = "patient";
	public static final String ACTIVITY_TAG_ADD_PATIENT = "add_patient";
	public static final String ACTIVITY_TAG_CASE_RESPONSE = "case_response";
	public static final String ACTIVITY_TAG_QUARANTINE_DISCHARGE = "quarantina";


	public static final String PATIENT_RESIDENCE = "residence";
	public static final String PATIENT_WORKPLACE = "workplace";
	public static final String PATIENT_CONFIRMED = "Confirmed";
	public static final String PATIENT_SUSPECTED = "Suspected";

	public static final String ACTIVITY_TAG_POSITIVE_LARVAE = "positive_larvae";
	public static final String PATIENT = "patient";
	public static final String POSITIVE_LARVAE = "larvae";

	public static final String TAG_CNIC = "1";
	public static final String TAG_PATIENT_NAME = "2";
	public static final int ACTIVITY_PATIENT = 10;
	public static final int ACTIVITY_CASE_RESPONSE = 10;

	public static final int ASYNC_TOWNS = 1;
	public static final int ASYNC_DIALOG = 2;
	public static final int ASYNC_HOTSPOT = 3;
	public static final int ASYNC_PATIENTS_LIST = 4;
	public static final int ASYNC_POSITIVE_LARVAE_LIST = 5;

	public static final int BT_RESIDENCE = 1;
	public static final int BT_WORKPLACE = 2;
	public static final int BT_NEW_ACTIVITY = 3;
	public static final int BT_SAVED_ACTIVITIES = 4;
	public static final int BT_SUBMIT_ACTIVITY = 5;
	public static final int BT_SAVE_ACTIVITY = 6;
	public static final int BT_CONFIRMED = 7;
	public static final int BT_SUSPECTED = 8;

	public static final int CATEGORY_YES_NO_LARVAE = 0;
	public static final int CATEGORY_POSITIVE_NEGATIVE_LARVAE = 1;
	public static final int CATEGORY_INDOOR_OUTDOOR = 2;
	public static final int CATEGORY_PUBLIC_PRIVATE = 3;
	public static final int CATEGORY_PATIENT = 4;
	public static final int CATEGORY_YES_NO_MOSQUITO = 5;
	public static final int CATEGORY_POSITIVE_NEGATIVE_MOSQUITO = 6;
	public static final int CATEGORY_IS_INSPECTOR = 7;
	public static final int CATEGORY_GPS_LOCATION = 8;
	public static final int CATEGORY_IRS_PATIENT = 9;
	public static final int CATEGORY_LARVAE_TAGGING_CSR = 10;
	public static final int CATEGORY_POSITIVE_LARVAE = 11;
	public static final int CATEGORY_DISINFECTION_SPRAY = 12;

	public static final float DISTANCE_DIFF_IN_METERS = 1000.0f;

	//	public static final ArrayList<String> listDesignation = new ArrayList<String>(Arrays.asList(new String[] { "AMO", "DMO" }));
	public static final ArrayList<String> listDesignationPins = new ArrayList<String>(Arrays.asList(new String[] { "2357", "3660" }));
//	public static final String PIN_DEFAULT = "2357";
//	public static final String PIN_PATIENT = "2357";

	public static final String FORMAT_DATE_TIME_SERVER = "yyyy-MM-dd HH:mm:ss";
}
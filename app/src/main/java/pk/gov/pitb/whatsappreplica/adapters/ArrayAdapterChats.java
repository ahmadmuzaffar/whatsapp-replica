package pk.gov.pitb.whatsappreplica.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import pk.gov.pitb.whatsappreplica.R;
import pk.gov.pitb.whatsappreplica.models.ClassChats;
import pk.gov.pitb.whatsappreplica.models.OnSwipeTouchListener;
import static pk.gov.pitb.whatsappreplica.helper.Globals.options;


public class ArrayAdapterChats extends RecyclerView.Adapter<ArrayAdapterChats.ViewHolder> {

    List<ClassChats> mList;
    Activity activity;
    int index;

    public ArrayAdapterChats(List<ClassChats> mList, Activity activity) {

        setHasStableIds(true);
        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public ArrayAdapterChats.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_chat_items, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = holder.getAdapterPosition();
            }
        });

        return holder;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(ArrayAdapterChats.ViewHolder viewHolder, int position) {
        index = position;

        viewHolder.textViewName.setText(mList.get(position).getName());
        viewHolder.textViewLastMessage.setText(mList.get(position).getLastMessage());
        viewHolder.textViewMessageTime.setText(mList.get(position).getMessageTime());

        if(position != 0){
            viewHolder.block.setBackground(activity.getResources().getDrawable(R.drawable.lists_border));
        }

        String imageToLoad = mList.get(position).getPictureURI();

        try {
            ImageLoader.getInstance().displayImage(imageToLoad, viewHolder.image, options, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout block, mainBlock;
        public TextView textViewName, textViewLastMessage, textViewMessageTime;
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.tv_name);
            textViewLastMessage = itemView.findViewById(R.id.tv_message);
            textViewMessageTime = itemView.findViewById(R.id.tv_time);
            image = itemView.findViewById(R.id.image);

            mainBlock = itemView.findViewById(R.id.main_block);
            block = itemView.findViewById(R.id.block);

        }
    }

    public void updateList(List<ClassChats> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}

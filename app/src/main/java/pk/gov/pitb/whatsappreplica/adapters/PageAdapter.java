package pk.gov.pitb.whatsappreplica.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import pk.gov.pitb.whatsappreplica.fragments.FragmentCalls;
import pk.gov.pitb.whatsappreplica.fragments.FragmentChats;
import pk.gov.pitb.whatsappreplica.fragments.FragmentStatus;


public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new FragmentChats();
            case 2:
                return new FragmentStatus();
            case 3:
                return new FragmentCalls();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}

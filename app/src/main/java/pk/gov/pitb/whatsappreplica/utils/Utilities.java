package pk.gov.pitb.whatsappreplica.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Base64;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pk.gov.pitb.whatsappreplica.helper.Constants;

public class Utilities
{
    public static void openCalender(final Context context, final EditText editText, final String minDate) {
        try{
            final int mYear, mMonth, mDay, mHour, mMinute;

            final Calendar c = Calendar.getInstance();
            final Calendar calendarMinDate = Calendar.getInstance();
            // final Calendar c = new GregorianCalendar(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
            if(minDate!=null){
                final SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP);
                Date mDate = sdf.parse(minDate);
                calendarMinDate.setTime(mDate);
            }

            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            if(minDate!=null) {
                                if(year<calendarMinDate.get(Calendar.YEAR) ||
                                        (year==calendarMinDate.get(Calendar.YEAR) && monthOfYear<calendarMinDate.get(Calendar.MONTH))
                                        || (year==calendarMinDate.get(Calendar.YEAR) && monthOfYear==calendarMinDate.get(Calendar.MONTH) && dayOfMonth<calendarMinDate.get(Calendar.DAY_OF_MONTH))){
                                    Toast.makeText(context, "Relieving date cannot be before arrival date", Toast.LENGTH_SHORT).show();
                                }else{
                                    c.set(year, monthOfYear, dayOfMonth);
                                    editText.setText(convertDateToString(c));
                                }
                            }else{
                                c.set(year, monthOfYear, dayOfMonth);
                                editText.setText(convertDateToString(c));
                            }
                        }
                    }, mYear, mMonth, mDay);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));

            long currentTimrInMillis = calendar.getTimeInMillis();
            datePickerDialog.getDatePicker().setMaxDate(currentTimrInMillis);
            System.out.println("max Date in milli :: " + currentTimrInMillis);

//            if(minDate!=null)
//                datePickerDialog.getDatePicker().setMinDate(convertDateStringToMillis(minDate));

            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String convertDateToDBString(Calendar calendar) {
        final SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP);
        final String strDate = sdf.format(calendar.getTime());
        return strDate;
    }
    public static String convertDateToString(Calendar calendar) {
        final SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP);
        final String strDate = sdf.format(calendar.getTime());
        return strDate;
    }

    public static long convertDateStringToMillis(String givenDateString){
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP);
        try {
            final Calendar c = Calendar.getInstance();
            Date mDate = sdf.parse(givenDateString);
            c.setTime(mDate);
            long timeInMilliseconds = mDate.getTime();

            c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
            c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
            c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));



            System.out.println("Date in milli :: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Date convertStringToDate(String stringDate) {

        if (stringDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
        Date date = null;
        try {
            date = sdf.parse(stringDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }
    /**
     * Convert date to simple string.
     *
     * @param sDate the s date
     * @return the string
     */
    public static String convertDateToSimpleString(Date sDate) {

        try {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "MMM dd, yyyy");
            final String strDate = dateFormat.format(sDate);
            return strDate;
        } catch (final Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }







    ////////////////////////////////////////////////// images //////////////////////////////////////////
    public static byte[] getBytes(Bitmap bitmap)
    {
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm");
        String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system

        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200 ,resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static byte[] getBytes(Bitmap resizedBitmap , String picturePath)
    {
        File file=new File(picturePath);
//    	bitmap=BitmapFactory.decodeFile(picturePath);

        //Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm");
        // String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
        String dateTime=sdf.format(file.lastModified());
        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200 ,resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static byte[] getSimpleBytes(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static Bitmap getImage(byte[] image)
    {
        try {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        }catch (OutOfMemoryError e){
            e.printStackTrace();
            return null;
        }
    }
    public static Bitmap ConvertPictureToBitmap(String filePath) throws FileNotFoundException {
        FileInputStream in = new FileInputStream(filePath);
        BufferedInputStream buf = new BufferedInputStream(in);
        Bitmap bmp = BitmapFactory.decodeStream(buf);

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bmp, bmp.getWidth() / 2, bmp.getHeight() / 2, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return resizedBitmap;
    }
    public static String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}
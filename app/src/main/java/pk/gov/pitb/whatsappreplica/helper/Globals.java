package pk.gov.pitb.whatsappreplica.helper;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pk.gov.pitb.whatsappreplica.R;


public class Globals {

	private static Globals instance = null;

	public static Globals getUsage() {
		return instance;
	}

	public static Globals getInstance(Activity activity) {
		if (instance == null) {
			instance = new Globals(activity);
		}
		instance.setActivityOnFront(activity);
		return instance;
	}

	public Locale mLocale;
	public Activity mActivity;
	public Context mContext;
	public String mPackageName;
	public int mScreenWidth;
	public int mScreenHeight;
	public static Typeface typeface = null;
	public static DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.logo)
			.showImageForEmptyUri(R.drawable.logo)
			.showImageOnFail(R.drawable.logo)
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.considerExifParams(true)
			.displayer(new SimpleBitmapDisplayer())
			.imageScaleType(ImageScaleType.EXACTLY)
			.resetViewBeforeLoading(true)
			.build();


	public Globals(Activity activity) {
		mLocale = Locale.ENGLISH;
		mActivity = activity;
		mContext = activity;
		mPackageName = mContext.getPackageName();
		Point size = new Point();
		mActivity.getWindowManager().getDefaultDisplay().getSize(size);
		mScreenWidth = size.x;
		mScreenHeight = size.y;
//		setUrduFont();
	}

	private void setActivityOnFront(Activity activity) {
		mActivity = activity;
		mContext = activity;
	}
	public static String getDateTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
		String value = sdf.format(date);
		return value;
	}
	public static String getAppVersion(){
		PackageInfo pInfo = null;
		try {
			pInfo = Globals.getUsage().mContext.getPackageManager().getPackageInfo(Globals.getUsage().mContext.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return pInfo.versionName;
	}

}